//QRCode Image Elements
var video = document.createElement("video");
var canvasEl = document.getElementById("canvas-video");  //Since we can't capture the raw data of a video, we must use a canvas
var canvas = canvasEl.getContext("2d");

var commands = [];
var controlCommands = ["stop", "start"];  //Some of these are for controlling the program, and should not be added

//Command information
var currentCommand;

class Timer {
    constructor(time, callback) {
        this.time = time;
        this.callback = callback;
        this.timeoutExists = false;
    }
    trigger() {
        if(!this.timeoutExists){
            this.timeoutExists = true;
            this.timeout = setTimeout(() => {
                //Just incase we need to add anything in the future
                this.callback();
                this.reset();
            }, this.time);
        }
    }   
    reset() {
        if(this.timeoutExists){
            clearTimeout(this.timeout);
            this.timeoutExists = false;
        }
    } 
}

class Countdown {
    constructor(intervalTime, maxCount, step, done) {
        this.intervalTime = intervalTime; //Time between counts
        this.maxCount = maxCount;
        this.step = step; //We need both something for each individual count, and after all is done
        this.done = done; //Incase for some reason we want this to run after countdown, maybe we can replace timer?
        this.started = false;
        this.counts = 0;
    }
    count() {
        if(this.counts < this.maxCount){
            this.counts++;
            this.step(this.counts);
        } else if(this.counts = this.maxCount) {
            this.reset();
            this.done();
        }
    }
    reset() {
        this.counts = 0;
        clearInterval(this.interval);
        this.started = false;
    }
    trigger() {
        if(!this.started) {
            this.started = true;
            this.count(); //We want to start counting immediately
            this.interval = setInterval(() => {
                this.count();
            }, this.intervalTime)
        }
    }
}

function clearDisplayCountdown() {
    $("#countdown").text("");
}

function setDisplayCountdown(count) {
    $("#countdown").text(`${count}`);
}

function addToProgramList(command) {
    $("#commands").append(`<li class="command"><img src="/img/${command}.png" /></li>`);
    $("#commands").append($("#commands").children('li').get().reverse());
}

//We must see the QR Code for 3 seconds before it comes permanent
var commandFinalize = new Timer(3000, () => {
    console.log("Finalize command", currentCommand);
    if(controlCommands.indexOf(currentCommand) > -1) {
        console.log(`Control: ${currentCommand}`);
    } else{
        commands.push(currentCommand);
        addToProgramList(currentCommand);
    }
});

//If we don't see a command for a second, don't finalize it
var clearCommand = new Timer(1000, () => {
    commandFinalize.reset();
    countdownAddCommand.reset();
    clearDisplayCountdown();
});

var countdownAddCommand = new Countdown(1000, 3, (count) => {
    console.log(`${4-count}...`);
    setDisplayCountdown(4-count);
}, () => {
    //Nothing here for now
});

navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment"}}).then((stream) => {
    //Add the video stream to the element
    video.srcObject = stream;
    video.setAttribute("playsinline", true);
    video.play();
    requestAnimationFrame(frame);
});

function frame() {
    //Canvas width and height
    var {width, height} = {width: canvasEl.width, height: canvasEl.height};
    //Draw the video to the canvas
    canvas.drawImage(video, 0, 0, width, height);
    var imageData = canvas.getImageData(0, 0, width, height); //Raw data from the video
    var qrCode = jsQR(imageData.data, imageData.width, imageData.height); //Process QR codes from video using library
    if(qrCode){
        currentCommand = qrCode.data;
        commandFinalize.trigger();
        countdownAddCommand.trigger();
        clearCommand.reset();
    } else {
        clearCommand.trigger();
    }
    requestAnimationFrame(frame);
}